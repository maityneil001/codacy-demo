<?php

App::uses('AppModel', 'Model');

/**
 * Contact Model
 *
 * @property User $Contact
 */
class Contact extends AppModel {
    // The Associations below have been created with all possible keys, those that are not needed can be removed
    
    public $actsAs = array('Containable');
    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public function removeContact($param) {
        $conditions=array(
            'user_id'=>$param['Notification']['sender_id'],
            'contact_id'=>$param['Notification']['reciever_id'],
        );
       return $this->deleteAll($conditions); 
    }
    public function acceptContact($param) {
        $conditions=array(
            'user_id'=>$param['Notification']['sender_id'],
            'contact_id'=>$param['Notification']['reciever_id'],
        );
       return $this->updateAll(array('accept'=>1),$conditions); 
    }
    public function newContact($param) {
        $data=array(
            'user_id'=>$param['Notification']['sender_id'],
            'contact_id'=>$param['Notification']['reciever_id'],
            'accept'=>1,
        );
       return $this->save($data); 
    }

}
