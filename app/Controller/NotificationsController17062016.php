<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class NotificationsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $uses=array('Memo');
    public $components = array('Paginator', 'PushNotification');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function api_removenotification($id = null) {
        $this->request->allowMethod('post', 'delete');
        $this->Notification->id = $id;
        $data = $this->Notification->findById($id);
        if ($data['Notification']['type'] == '2') {
            $this->Contact->removeContact($data);
        }
        if ($ret = $this->Notification->delete()) {
            $res['flag'] = 'S';
        } else {
            $res['flag'] = 'F';
        }
        $res['msg'] = 'Notification removed';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

    public function api_acceptnotification($id = null) {
        $this->request->allowMethod('post', 'get');
        $this->Notification->id = $id;
        $data = $this->Notification->findById($id);
        if ($data['Notification']['type'] == '2') {
            $this->Contact->acceptContact($data);
        }
        if ($ret = $this->Notification->delete()) {
            $res['flag'] = 'S';
        } else {
            $res['flag'] = 'F';
        }
        $res['msg'] = 'Notification accepted';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

    public function api_accepteventnotification($id = null) {
        $this->request->allowMethod('post', 'get');
        $this->Notification->id = $id;
        $data = $this->Notification->findById($id);
        if ($data['Notification']['type'] == '1') {
            $this->Event->acceptEvent($data);
        }
        //die;
        if ($ret = $this->Notification->delete()) {
            $res['flag'] = 'S';
        } else {
            $res['flag'] = 'F';
        }
        $res['msg'] = 'Notification accepted';
        $res['document'] = $data;
        $this->setSerialize($res);
    }
    public function api_acceptmemonotification($id = null) {
        $this->request->allowMethod('post', 'get');
        $this->Notification->id = $id;
        $data = $this->Notification->findById($id);
        if ($data['Notification']['type'] == '3') {
            $this->Memo->acceptMemo($data);
        }
        //die;
        if ($ret = $this->Notification->delete()) {
            $res['flag'] = 'S';
        } else {
            $res['flag'] = 'F';
        }
        $res['msg'] = 'Notification accepted';
        $res['document'] = $data;
        $this->setSerialize($res);
    }

    public function api_addeventinvitation() {
        $this->request->allowMethod('post', 'get');
        $notificationdata = array(
            'reciever_id' => $this->request->data('contact_id'),
            'title' => 'Event request',
            'sender_id' => $this->request->data('user_id'),
            'type' => '1',
            'ontime' => time(),
            'event_id' => $this->request->data('event_id'),
        );
        $ret = $this->Notification->save($notificationdata);
        $id = $this->Notification->getInsertId();
        $token = $this->Token->find('all',array('conditions'=>array('user_id'=>$this->request->data('contact_id'))));
        $event = $this->Event->findById($this->request->data('event_id'));
        $user = $this->User->findById($this->request->data('user_id'));
        foreach ($token as $val) {
            $notifyarray = array(
                'id' => $id,
                'device_type' => $val['Token']['type'],
                'message' => $user['User']['displayname'] .' have send you request for '.$event['Event']['title'] ,
                'deviceToken' => $val['Token']['token'],
            );
            $this->PushNotification->sendPush($notifyarray);
        }

        $res['flag'] = 'S';
        $res['msg'] = 'Contact saved';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_addmemoinvitation() {
        $this->request->allowMethod('post', 'get');
        $notificationdata = array(
            'reciever_id' => $this->request->data('contact_id'),
            'title' => 'Memo request',
            'sender_id' => $this->request->data('user_id'),
            'type' => '3',
            'ontime' => time(),
            'event_id' => $this->request->data('memo_id'),
        );
        $ret = $this->Notification->save($notificationdata);
        $id = $this->Notification->getInsertId();
        $token = $this->Token->find('all',array('conditions'=>array('user_id'=>$this->request->data('contact_id'))));
        $event = $this->Memo->findById($this->request->data('memo_id'));
        $user = $this->User->findById($this->request->data('user_id'));
        foreach ($token as $val) {
            $notifyarray = array(
                'id' => $id,
                'device_type' => $val['Token']['type'],
                'message' => $user['User']['displayname'] .' have send you request for '.$event['Memo']['title'] ,
                'deviceToken' => $val['Token']['token'],
            );
            $this->PushNotification->sendPush($notifyarray);
        }

        $res['flag'] = 'S';
        $res['msg'] = 'Memo notification';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_eventnotifications($sender,$event) {
        $query="select reciever_id from notifications where type='1' and sender_id='".$sender."' and event_id='".$event."'";
        $ret=$this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Contact saved';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_memonotifications($sender,$memo) {
        $query="select reciever_id from notifications where type='3' and sender_id='".$sender."' and event_id='".$memo."'";
        $ret=$this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Contact saved';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

}
