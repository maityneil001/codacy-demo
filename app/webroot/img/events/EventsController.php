<?php

App::uses('AppController', 'Controller');

/**
 * Events Controller
 *
 * @property Event $Event
 * @property PaginatorComponent $Paginator
 */
class EventsController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
    }
    public $virtualFields=array(
        'createdby'=>'Events.user_id'
    );
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Event->recursive = 0;
        $this->set('events', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Event->exists($id)) {
            throw new NotFoundException(__('Invalid event'));
        }
        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
        $this->set('event', $this->Event->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Event->create();
            if ($this->Event->save($this->request->data)) {
                $this->Flash->success(__('The event has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The event could not be saved. Please, try again.'));
            }
        }
        $users = $this->Event->User->find('list');
        $types = $this->Event->Type->find('list');
        $users = $this->Event->User->find('list');
        $this->set(compact('users', 'types', 'users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Event->exists($id)) {
            throw new NotFoundException(__('Invalid event'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Event->save($this->request->data)) {
                $this->Flash->success(__('The event has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The event could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
            $this->request->data = $this->Event->find('first', $options);
        }
        $users = $this->Event->User->find('list');
        $types = $this->Event->Type->find('list');
        $users = $this->Event->User->find('list');
        $this->set(compact('users', 'types', 'users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Event->id = $id;
        if (!$this->Event->exists()) {
            throw new NotFoundException(__('Invalid event'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Event->delete()) {
            $this->Flash->success(__('The event has been deleted.'));
        } else {
            $this->Flash->error(__('The event could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function api_create() {
        if ($this->request->is('post')) {
            if($this->request->data('id')){
                $res = $this->Event->createAndUpdateEvent($this->request->data,$this->request->data('id'));
            }else{
                $res = $this->Event->createAndUpdateEvent($this->request->data);
            }
            $this->setSerialize($res);
        }
    }

    public function api_forToday() {
        $res = $this->Event->getTodayEvents();
        $this->setSerialize($res);
    }
    public function api_allEvents() {
        $fromdate = $this->request->data('fromdate');
        $todate = $this->request->data('todate');
        $res = $this->Event->getAllEventsDatewise($fromdate,$todate);
        $this->setSerialize($res);
    }
    public function api_eventDetails($id) {
        //$this->Event->id=$id;
        $ret=$this->Event->findById($id);
        $res['flag'] = 'S';
        $res['msg'] = 'Event Image';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_uploadEventImg() {
        $data=$this->request->data('dataitem');
        $ret=$this->dataToImg('events', $data['image']);
        $this->File->insertFiledata($ret,$data);
        $res['flag'] = 'S';
        $res['msg'] = 'Event Image';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_eventusers($id) {
        $query = "SELECT * from events_users where event_id='".$id."' ";
        $token = $this->Event->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Event users list';
        $res['document'] = $token;
        $this->setSerialize($res);
    }
    public function api_eventfiles($id) {
         $query = "SELECT * from files where event_id='".$id."' ";
        $files = $this->Event->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Event files list';
        $res['document'] = $files;
        $this->setSerialize($res);
    }
    public function api_delete($id = null) {
        $this->Event->id = $id;
        $this->request->allowMethod('post', 'delete');
        if ($this->Event->delete()) {
            $res['flag'] = 'S';
            $res['msg'] = 'Event deleted';
            $res['document'] = $id;
        } else {
            $res['flag'] = 'E';
            $res['msg'] = 'Event not deleted';
            $res['document'] = false;
        }
        $this->setSerialize($res);
    }
    public function api_deletefile($id = null) {
        $query="Delete from files where id='".$id."'";       
        //$this->request->allowMethod('post', 'delete');
        if ($this->Event->query($query)) {
            $res['flag'] = 'S';
            $res['msg'] = 'File deleted';
            $res['document'] = $id;
        } else {
            $res['flag'] = 'E';
            $res['msg'] = 'File could not be deleted';
            $res['document'] = false;
        }
        $this->setSerialize($res);
    }
    public function api_deleteeventuser($userid = null,$eventid = null) {
        $query="delete from events_users where user_id='".$userid."' and event_id='".$eventid."'";  
        //$this->request->allowMethod('post', 'delete');
        /* debug($this->Event->query($query));die;
        if ($this->Event->query($query)) {
            $res['flag'] = 'S';
            $res['msg'] = 'User deleted';
            $res['document'] = $userid;
        } else {
            $res['flag'] = 'E';
            $res['msg'] = 'User could not be deleted';
            $res['document'] = false;
        }*/
        $this->Event->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'User deleted';
        $res['document'] = $userid;
        $this->setSerialize($res);
    }
    public function api_getalluserevent() {
        $data=$this->request->data;
        $fromdate = $this->request->data('fromdate');
        $todate = $this->request->data('todate');
        $user = $this->request->data('user');
        $query="SELECT events.* FROM events left join events_users on events.id=events_users.event_id where `fromdate` <='" . $todate . "' AND `todate`>= '" . $fromdate . "' AND events_users.user_id='" . $user . "'";
        //echo $query;die();
        $events = $this->Event->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Userevents Lists';
        $res['document'] = $events;
        $this->setSerialize($res);
    }
    public function api_addupdatenote() {
        $data=$this->request->data;
        if(isset($data['id']) && $data['id']!=''){
            $this->Note->id=$data['id'];
        }
        $notes=$this->Note->save(array('event_id'=>$data['event_id'],'user_id'=>$data['user_id'],'note'=>$data['note']));
        $res['flag'] = 'S';
        $res['msg'] = 'Note saved';
        $res['document'] = $notes;
        $this->setSerialize($res);
    }

}
