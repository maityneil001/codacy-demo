<?php

App::uses('AppController', 'Controller');
App::uses('Emoji', 'Lib');
/**
 * Events Controller
 *
 * @property Event $Event
 * @property PaginatorComponent $Paginator
 */
class EventsController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
    }
    public $uses=array('Memo','MemoNote','MemoFile','MemoUser','Notification','File');

    public $virtualFields=array(
        'createdby'=>'Events.user_id'
    );
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator','PushNotification');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Event->recursive = 0;
        $this->set('events', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Event->exists($id)) {
            throw new NotFoundException(__('Invalid event'));
        }
        $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
        $this->set('event', $this->Event->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Event->create();
            if ($this->Event->save($this->request->data)) {
                $this->Flash->success(__('The event has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The event could not be saved. Please, try again.'));
            }
        }
        $users = $this->Event->User->find('list');
        $types = $this->Event->Type->find('list');
        $users = $this->Event->User->find('list');
        $this->set(compact('users', 'types', 'users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Event->exists($id)) {
            throw new NotFoundException(__('Invalid event'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Event->save($this->request->data)) {
                $this->Flash->success(__('The event has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The event could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
            $this->request->data = $this->Event->find('first', $options);
        }
        $users = $this->Event->User->find('list');
        $types = $this->Event->Type->find('list');
        $users = $this->Event->User->find('list');
        $this->set(compact('users', 'types', 'users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Event->id = $id;
        if (!$this->Event->exists()) {
            throw new NotFoundException(__('Invalid event'));
        }
        $this->request->allowMethod('post', 'delete');

        if ($this->Event->delete()) {
            $this->Flash->success(__('The event has been deleted.'));
        } else {
            $this->Flash->error(__('The event could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function api_create() {
        if ($this->request->is('post')) {
          $msg='';
          $msg1='';
            if($this->request->data('id')){
                $evdetails = $this->Event->findById($this->request->data('id'));
                if($evdetails['Event']['fromdate']!=$this->request->data('fromdate') || $evdetails['Event']['todate']!=$this->request->data('todate') || $evdetails['Event']['fromtime']!=$this->request->data('fromtime') || $evdetails['Event']['totime']!= $this->request->data('totime')){
                  $msg="changed time of the event ";
                  $msg1= " to " . date('F,d Y',($this->request->data('fromdate')/1000)) .','.$this->request->data('fromtime').'-'.$this->request->data('totime');
                }elseif($this->request->data('title')!=$evdetails['Event']['title']){
                  $msg="renamed the event ";
                  $msg1 = " to ".$this->request->data('title');
                }else{
                    $msg="updated the event ";
                }
                $res = $this->Event->createAndUpdateEvent($this->request->data,$this->request->data('id'));
                $this->sendUpdatenotification($msg,"",$this->request->data('id'),$msg1);
            }else{
                $res = $this->Event->createAndUpdateEvent($this->request->data);
            }
            $this->setSerialize($res);
        }
    }

    public function api_forToday() {
        $res = $this->Event->getTodayEvents();
        $this->setSerialize($res);
    }
    public function api_allEvents() {
        $fromdate = $this->request->data('fromdate');
        $todate = $this->request->data('todate');
        $res = $this->Event->getAllEventsDatewise($fromdate,$todate);
        $this->setSerialize($res);
    }
    public function api_eventDetails($id) {
        //$this->Event->id=$id;
        $ret=$this->Event->findById($id);
        $res['flag'] = 'S';
        $res['msg'] = 'Event Image';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_uploadEventImg() {
        $data=$this->request->data('dataitem');
        $ret=$this->dataToImg('events', $data['image']);
        $this->File->insertFiledata($ret,$data);
        $this->sendUpdatenotification("added new image to event ",$data['user'],$data['event']);
        $res['flag'] = 'S';
        $res['msg'] = 'Event Image';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_eventusers($id) {
        $query = "SELECT * from events_users where event_id='".$id."' ";
        $token = $this->Event->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Event users list';
        $res['document'] = $token;
        $this->setSerialize($res);
    }
    public function api_alleventusers() {
        $datavalue=$this->request->data('datavalue');
        $query = "SELECT * from events_users where event_id IN (".$datavalue.") ";
        $token = $this->Event->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Event users list';
        $res['document'] = $token;
        $this->setSerialize($res);
    }
    public function api_eventfiles($id) {
         $query = "SELECT * from files where event_id='".$id."' ";
        $files = $this->Event->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Event files list';
        $res['document'] = $files;
        $this->setSerialize($res);
    }
    public function api_delete($id = null) {
        $this->Event->id = $id;
        $this->request->allowMethod('post', 'delete','get');
        $evdetails = $this->Event->findById($id);
        //pr($evdetails);die;
        $msg="scheduled for ". date('M,d Y',($evdetails['Event']['fromdate']/1000)) .','.$evdetails['Event']['fromtime'];
        $this->sendUpdatenotification("deleted the event ","",$id ,$msg) ;
        $query="delete from notifications where event_id='$id' and (type='1' or type='4')";
        $this->Notification->query($query);
        if ($this->Event->delete()) {
            $res['flag'] = 'S';
            $res['msg'] = 'Event deleted';
            $res['document'] = $id;
        } else {
            $res['flag'] = 'E';
            $res['msg'] = 'Event not deleted';
            $res['document'] = false;
        }
        $this->setSerialize($res);
    }
    public function api_deletefile() {
        $id=$this->request->data('id');
        $event=$this->request->data('event');
        $user=$this->request->data('user');
        $query="Delete from files where id='".$id."'";
        $details=" deleted a file from the event ";
        //$this->sendUpdatenotification($details,$user,$event);
        $this->File->id = $id;
        if ($this->File->delete()) {
          $this->sendUpdatenotification($details,$user,$event);
            $res['flag'] = 'S';
            $res['msg'] = 'File deleted';
            $res['document'] = $id;
        } else {
            $res['flag'] = 'E';
            $res['msg'] = 'File could not be deleted';
            $res['document'] = false;
        }
        $this->setSerialize($res);
    }
    public function api_deleteeventuser($userid = null,$eventid = null) {
        $query="delete from events_users where user_id='".$userid."' and event_id='".$eventid."'";
        //$this->request->allowMethod('post', 'delete');
        /* debug($this->Event->query($query));die;
        if ($this->Event->query($query)) {
            $res['flag'] = 'S';
            $res['msg'] = 'User deleted';
            $res['document'] = $userid;
        } else {
            $res['flag'] = 'E';
            $res['msg'] = 'User could not be deleted';
            $res['document'] = false;
        }*/
        $this->Event->query($query);
        $evdetails = $this->Event->findById($eventid);
        $user=$evdetails['Event']['user_id'];
        $userdetails = $this->User->findById($user);
        $insarray=array(
              'type'=>7,
              'reciever_id'=>$userid,
              'title'=>$userdetails['User']['displayname']." removed you from the event " . $evdetails['Event']['title'] ,
              'isview'=>0,
              'event_id'=>$eventid,'sender_id'=>$user,'ontime'=>time());
          $this->Notification->save($insarray);
          $token = $this->Token->find('all', array('conditions' => array('user_id' => $userid)));
          foreach ($token as $val) {
              $notifyarray = array(
                  'id' => $eventid,
                  'device_type' => $val['Token']['type'],
                  'message' => $userdetails['User']['displayname']." removed you from the event " . $evdetails['Event']['title'] ,
                  'deviceToken' => $val['Token']['token'],
              );
              $this->PushNotification->sendPush($notifyarray);
          }

        $res['flag'] = 'S';
        $res['msg'] = 'User deleted';
        $res['document'] = $userid;
        $this->setSerialize($res);
    }
    public function api_getalluserevent() {
        $data=$this->request->data;
        $fromdate = $this->request->data('fromdate');
        $todate = $this->request->data('todate');
        $user = $this->request->data('user');
        $query="SELECT events.* FROM events left join events_users on events.id=events_users.event_id where `fromdate` <='" . $todate . "' AND `todate`>= '" . $fromdate . "' AND events_users.user_id='" . $user . "'";
        //echo $query;die();
        $events = $this->Event->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Userevents Lists';
        $res['document'] = $events;
        $this->setSerialize($res);
    }
    public function api_addupdatenote() {
        $data=$this->request->data;
        if(isset($data['id']) && $data['id']!=''){
            $this->Note->id=$data['id'];
        }
        $text=Emoji::Encode($data['note']);

        $notes=$this->Note->save(array('event_id'=>$data['event_id'],'user_id'=>$data['user_id'],'note'=>$text));
        $this->sendUpdatenotification("updated notes for the event",$data['user_id'],$data['event_id']);
        $res['flag'] = 'S';
        $res['msg'] = 'Note saved';
        $res['document'] = $notes;
        $this->setSerialize($res);
    }
    public function api_getallnote($id=null) {
       $eventnote = "SELECT users.id,notes.id as noteid,users.username,users.displayname,notes.note FROM notes JOIN users ON notes.user_id=users.id where notes.event_id='" .$id. "'";
       //Emoji::Decode($text);
       $notes=$this->Note->query($eventnote);
       foreach ($notes as $key => $value) {
         //print_r($value);die;
         $notes[$key]['notes']['note']=Emoji::Decode($value['notes']['note']);
       }
       $res['flag'] = 'S';
       $res['msg'] = 'Note list';
       $res['document'] = $notes;
       $this->setSerialize($res);
    }
    public function api_convertextraparams() {
       $data=$this->request->data;
       /***notes****/
       $memonote = "SELECT * FROM memo_notes  where memo_id='" .$data['memo']. "'";
       $notes=$this->MemoNote->query($memonote);
       $this->MemoNote->query("DELETE  FROM memo_notes  where memo_id='" .$data['memo']. "'");
       if(!empty($notes)){
           foreach ($notes as $value) {
                $value['memo_notes']['event_id']=$data['event'];
                $this->Note->save($value['memo_notes']);
            }
       }

       /*****images****/
       $memofiles ="SELECT * from memo_files where memo_id='".$data['memo']."' ";
       $files=$this->MemoFile->query($memofiles);
       $this->MemoFile->query("DELETE  FROM memo_files  where memo_id='" .$data['memo']. "'");
       if(!empty($files)){
        foreach ($files as $value) {
            $value['memo_files']['event_id']=$data['event'];
            $this->File->save($value['memo_files']);
        }
       }
       /*****users****/
       $memousers = "SELECT * FROM memo_users  where memo_id='" .$data['memo']. "'";
       $users=$this->Memo->query($memousers);
       $this->Memo->query("DELETE  FROM memo_users  where memo_id='" .$data['memo']. "'");
       foreach ($users as $value) {
           //$value['memo_users']['event_id']=$data['event'];
           $this->User->query("UPDATE  `users` SET  `need_sync` =  '1',`event` =  '1' WHERE  `users`.`id` =".$value['memo_users']['user_id']);
           $this->Event->addeventuser($data['event'],$value['memo_users']['user_id']);
       }
       $this->Memo->query("DELETE  FROM memos  where id='" .$data['memo']. "'");
       $nquery="update notifications set event_id='".$data['event']."',type='1' where event_id='".$data['memo']."' and type='3'";
       $this->Notification->query($nquery);
       $nquery1="update notifications set event_id='".$data['event']."',type='4' where event_id='".$data['memo']."' and type='5'";
       $this->Notification->query($nquery1);
       $res['flag'] = 'S';
       $res['msg'] = 'Note list';
       $res['document'] = true;
       $this->setSerialize($res);
    }
    public function sendUpdatenotification($details, $user, $event , $msg="") {
        //$this->Event->id=$event;
        //$this->autoRender=false;
        //echo $details;die;
        $evdetails = $this->Event->findById($event);
        if($user==""){
          $user=$evdetails['Event']['user_id'];
        }
        $userdetails = $this->User->findById($user);
        //pr($userdetails);die;
        $eveusersquery = "select * from events_users where event_id='" . $event . "' and user_id !='" . $user . "'";
        //$eveusersquery = "select * from events_users where event_id='" . $event."'";
        $eveusers = $this->Event->query($eveusersquery);
        foreach ($eveusers as $usr) {
          $this->User->query("update users set need_sync=1,event=1 where id=".$usr['events_users']['user_id']);
          $insarray=array(
                'type'=>6,
                'reciever_id'=>$usr['events_users']['user_id'],
                'title'=>$userdetails['User']['displayname']." " . $details." " . (($details!='renamed the event ')?$evdetails['Event']['title']:'') . " " .$msg,
                'isview'=>0,
                'event_id'=>$event,'sender_id'=>$user,'ontime'=>time());
          if(strpos($details,'deleted')!== false){
            $insarray['type']=7;
          }
          $this->Notification->save($insarray);
            $token = $this->Token->find('all', array('conditions' => array('user_id' => $usr['events_users']['user_id'])));
            foreach ($token as $val) {
                $notifyarray = array(
                    'id' => $event,
                    'device_type' => $val['Token']['type'],
                    'message' => $userdetails['User']['displayname']." " . $details." " . (($details!='renamed the event ')?$evdetails['Event']['title']:'') . " " .$msg,
                    'deviceToken' => $val['Token']['token'],
                );
                $this->PushNotification->sendPush($notifyarray);
            }
        }
    }
    public function api_leaveEvent(){
        $event=$this->request->data('event');
        $user=$this->request->data('user');
        $evdetails = $this->Event->findById($event);
        $userdetails = $this->User->findById($user);
        $details=" has left the event ";
        $this->Event->query("delete from events_users where user_id=$user and event_id=$event");
        $this->sendUpdatenotification($details,$user,$event);
        /*$this->Event->query("delete from events_users where user_id=$user and event_id=$event");
        $this->User->query("update users set need_sync=1,event=1 where id=".$usr['events_users']['user_id']);
        $insarray=array(
              'type'=>10,
              'reciever_id'=>$evdetails['Event']['user_id'],
              'title'=>$userdetails['User']['displayname']." " . $details." " . $evdetails['Event']['title'] ,
              'isview'=>0,
              'event_id'=>$memo,'sender_id'=>$user,'ontime'=>time());
          $this->Notification->save($insarray);
          $token = $this->Token->find('all', array('conditions' => array('user_id' => $evdetails['Event']['user_id'])));
          foreach ($token as $val) {
              $notifyarray = array(
                  'id' => $memo,
                  'device_type' => $val['Token']['type'],
                  'message' => $userdetails['User']['displayname']." " . $details." " . $evdetails['Event']['title'],
                  'deviceToken' => $val['Token']['token'],
              );
              $this->PushNotification->sendPush($notifyarray);
          }*/
          $res['flag'] = 'S';
          $res['msg'] = 'Left event';
          $res['document'] = true;
          $this->setSerialize($res);
    }
    public function api_sendUpdatenotification($details, $user, $event , $msg="",$memo) {
        //$this->Event->id=$event;
        //echo $details;die;
        $this->autoRender=false;
        $evdetails = $this->Event->findById($event);
        if($user==""){
          $user=$evdetails['Event']['user_id'];
        }
        $userdetails = $this->User->findById($user);
        //pr($userdetails);die;
        $eveusersquery = "select * from events_users where event_id='" . $event . "' and user_id !='" . $user . "'";
        //$eveusersquery = "select * from events_users where event_id='" . $event."'";
        $eveusers = $this->Event->query($eveusersquery);
        foreach ($eveusers as $usr) {
          $insarray=array(
                'type'=>10,
                'reciever_id'=>$usr['events_users']['user_id'],
                'title'=>$userdetails['User']['displayname']." " . $details." " . $evdetails['Event']['title'] . " " .$msg,
                'isview'=>0,
                'event_id'=>$memo,'sender_id'=>$user,'ontime'=>time());
            //print_r($insarray);die;
            $this->Notification->save($insarray);
            $token = $this->Token->find('all', array('conditions' => array('user_id' => $usr['events_users']['user_id'])));
            foreach ($token as $val) {
                $notifyarray = array(
                    'id' => $memo,
                    'device_type' => $val['Token']['type'],
                    'message' => $userdetails['User']['displayname']." " . $details." " . $evdetails['Event']['title'] . " " .$msg,
                    'deviceToken' => $val['Token']['token'],
                );
                $this->PushNotification->sendPush($notifyarray);
            }
        }
    }

}
