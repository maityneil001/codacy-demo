<?php

App::uses('AppModel', 'Model');

/**
 * Event Model
 *
 * @property User $User
 * @property Type $Type
 * @property File $File
 * @property Note $Note
 * @property User $User
 */
class EventUser extends AppModel {

    public $actAs = array('Containable');

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';


    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Event' => array(
            'className' => 'Event',
            'foreignKey' => 'event_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
