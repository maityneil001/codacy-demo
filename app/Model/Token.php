<?php

App::uses('AppModel', 'Model');

/**
 * Notification Model
 *
 */
class Token extends AppModel {

    public $actAs = array('Containable');

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';

}
