<?php

App::uses('AppModel', 'Model');

/**
 * Note Model
 *
 * @property Memo $Memo
 * @property User $User
 */
class MemoNote extends AppModel {

    public $actAs = array('Containable');

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Memo' => array(
            'className' => 'Memo',
            'foreignKey' => 'memo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
