<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array('Paginator', 'Email', 'Session', 'RequestHandler');
    public $uses = array('User', 'Contact', 'File', 'Note', 'Event', 'Notification', 'Token');

    public function beforeFilter() {
        if ($this->params['prefix'] == 'api') {
            
        }
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Methods', '*');
        $this->response->header('Access-Control-Allow-Headers', 'X-Requested-With');
    }

    public function setSerialize($res) {

        $this->set(array(
            'response' => array($res),
            '_serialize' => array('response')
        ));
    }

    public function dataToImg($dest_dir, $data) {
        //App::import('Vendor', 'ImageTool');
        $data = base64_decode($data);
        $imgName = time() . rand() . '.png';
        //$thumbName = time() . rand() . '_thumb.png';
        $uploadpath = WWW_ROOT . 'img' . DS . $dest_dir . DS . $imgName;
        $relativePath = IMAGE_DIRECTORY . $dest_dir . DS . $imgName;
        if (file_put_contents($uploadpath, $data)) {
            /*$thumbPath = IMAGE_DIRECTORY . $dest_dir . DS . $thumbName;
            $status = ImageTool::resize(array(
                        'input' => $uploadpath,
                        'output' => $thumbPath,
                        'width' => 100,
                        'height' => 100
            ));*/
            return $relativePath;
        } else {
            return false;
        }
    }

}
