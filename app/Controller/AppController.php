<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array('Paginator', 'Email', 'Session', 'RequestHandler');
    public $uses = array('User', 'Contact', 'File', 'Note', 'Event', 'Notification', 'Token');

    public function beforeFilter() {
        if ($this->params['prefix'] == 'api') {

        }
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->header('Access-Control-Allow-Methods', '*');
        $this->response->header('Access-Control-Allow-Headers', 'X-Requested-With');
        $this->response->header('Access-Control-Allow-Headers', 'Content-Type');
    }

    public function setSerialize($res) {

        $this->set(array(
            'response' => array($res),
            '_serialize' => array('response')
        ));
    }

    public function dataToImg($dest_dir, $data) {
        //App::import('Vendor', 'ImageTool');
        $data = base64_decode($data);
        $basename=time() . rand();
        $imgName = $basename . '.png';
        $thumbName = $basename . '_thumb.png';
        $uploadpath = WWW_ROOT . 'img' . DS . $dest_dir . DS . $imgName;
        $thumbpath = WWW_ROOT . 'img' . DS . $dest_dir . DS . $thumbName;
        $relativePath = IMAGE_DIRECTORY . $dest_dir . DS . $imgName;
        $relativeThumbPath = IMAGE_DIRECTORY . $dest_dir . DS . $thumbName;
        if (file_put_contents($uploadpath, $data)) {
            $this->Thumbnail($relativePath, $thumbpath, 100);
            /* $thumbPath = IMAGE_DIRECTORY . $dest_dir . DS . $thumbName;
              $status = ImageTool::resize(array(
              'input' => $uploadpath,
              'output' => $thumbPath,
              'width' => 100,
              'height' => 100
              )); */
            return $relativeThumbPath;
        } else {
            return false;
        }
    }

    public function Thumbnail($url, $filename, $width = 150, $height = true) {
        // download and create gd image
        $image = ImageCreateFromString(file_get_contents($url));
        // calculate resized ratio
        // Note: if $height is set to TRUE then we automatically calculate the height based on the ratio
        $height = $height === true ? (ImageSY($image) * $width / ImageSX($image)) : $height;
        // create image
        $output = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($output, $image, 0, 0, 0, 0, $width, $height, ImageSX($image), ImageSY($image));
        // save image
        ImageJPEG($output, $filename, 100);
        // return resized image
        return $output; // if you need to use it
    }
    public function updateUsersync($user=array(),$field=null){
        $this->User->updateAll(array($field=>1),array('id'=>$user));
        return true;
    }

}
