<?php

App::uses('AppController', 'Controller');

/**
 * Contacts Controller
 *
 * @property Contact $Contact
 * @property PaginatorComponent $Paginator
 */
class CountriesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Country->recursive = 0;
        $this->set('contacts', $this->Paginator->paginate());
    }
    public function api_countryList() {
        $res = $this->Country->countryList();
        $this->setSerialize($res);
    }

}
