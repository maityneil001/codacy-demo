<?php

App::uses('AppController', 'Controller');

/**
 * Contacts Controller
 *
 * @property Contact $Contact
 * @property PaginatorComponent $Paginator
 */
class ContactsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator','PushNotification');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Contact->recursive = 0;
        $this->set('contacts', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Contact->exists($id)) {
            throw new NotFoundException(__('Invalid contact'));
        }
        $options = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
        $this->set('contact', $this->Contact->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Contact->create();
            if ($this->Contact->save($this->request->data)) {
                $this->Flash->success(__('The contact has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The contact could not be saved. Please, try again.'));
            }
        }
        $users = $this->Contact->User->find('list');
        $this->set(compact('users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Contact->exists($id)) {
            throw new NotFoundException(__('Invalid contact'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Contact->save($this->request->data)) {
                $this->Flash->success(__('The contact has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The contact could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
            $this->request->data = $this->Contact->find('first', $options);
        }
        $users = $this->Contact->User->find('list');
        $this->set(compact('users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Contact->id = $id;
        if (!$this->Contact->exists()) {
            throw new NotFoundException(__('Invalid contact'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Contact->delete()) {
            $this->Flash->success(__('The contact has been deleted.'));
        } else {
            $this->Flash->error(__('The contact could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function api_addcontact() {
        if ($this->request->is('post')) {
            $query = "select * from contacts where (contact_id=" . $this->request->data('contact_id') . " AND user_id=" . $this->request->data('user_id') . ") OR (contact_id=" . $this->request->data('user_id') . " AND user_id=" . $this->request->data('contact_id') . ")";
            //echo $query;die;
            $rows = $this->Contact->query($query);
            /* pr($rows);die;
              $rows=$this->Contact->find('first',array('conditions'=>array(
              'OR'=>array(
              'AND'=>array('contact_id'=>$this->request->data('contact_id'),'user_id'=> $this->request->data('user_id')),
              'AND'=>array('contact_id'=>$this->request->data('user_id'),'user_id'=> $this->request->data('contact_id')),
              )
              ), 'contain' => false));
              pr($this->Contact->lastQuery());die; */
            if (!empty($rows)) {
                $res['flag'] = 'E';
                $res['msg'] = 'Contact already exists or pending request';
                $res['document'] = FALSE;
            } else {
                if ($ret = $this->Contact->save($this->request->data)) {
                    $notificationdata = array(
                        'type' => '1',
                        'reciever_id' => $this->request->data('contact_id'),
                        'title' => 'Contact request',
                        'sender_id' => $this->request->data('user_id'),
                        'type' => '2',
                        'ontime' => time(),
                    );
                    $this->Notification->save($notificationdata);
                    $id = $this->Notification->getInsertId();
                    $token = $this->Token->find('all',array('conditions'=>array('user_id'=>$this->request->data('contact_id'))));
                    $user = $this->User->findById($this->request->data('user_id'));
                    //pr($token);die;
                    foreach ($token as $val) {
                        $notifyarray = array(
                            'id' => $id,
                            'device_type' => $val['Token']['type'],
                            'message' => $user['User']['displayname'] . ' have send you  contact request.',
                            'deviceToken' => $val['Token']['token'],
                        );
                        $this->PushNotification->sendPush($notifyarray);
                    }
                    $res['flag'] = 'S';
                    $res['msg'] = 'Contact saved';
                    $res['document'] = $ret;
                }
            }

            $this->setSerialize($res);
        }
    }

    public function api_getcontacts($id) {
        $options = array('conditions' => array('OR' => array('Contact.user_id' => $id, 'Contact.contact_id' => $id), 'AND' => array('accept' => '1')), 'contain' => false);
        $ret = $this->Contact->find('all', $options);
        $res['flag'] = 'S';
        $res['msg'] = 'Contact list';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_removecontact($id) {
        $this->Contact->id=$id;
        $ret=$this->Contact->delete();
        $res['flag'] = 'S';
        $res['msg'] = 'Contact list';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

}
