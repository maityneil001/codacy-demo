<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class NotificationsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $uses = array('Memo', 'Event', 'Token', 'User', 'Notification');
    public $components = array('Paginator', 'PushNotification');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function api_removenotification($id = null) {
        $this->request->allowMethod('post', 'delete');
        $this->Notification->id = $id;
        $data = $this->Notification->findById($id);
        if ($data['Notification']['type'] == '2') {
            $this->Contact->removeContact($data);
        }
        if($data['Notification']['type'] =='1' || $data['Notification']['type'] == '4'){
            $this->acceptRejectnotification("did not join the event ", $data['Notification']['reciever_id'], $data['Notification']['event_id'], 1, $data,true);
        }elseif($data['Notification']['type'] =='2'){
            $this->acceptRejectnotification("has not accepted your contact request ", $data['Notification']['reciever_id'], 0, 3, $data,true);
        }elseif($data['Notification']['type'] =='3' || $data['Notification']['type'] == '5'){
            $this->acceptRejectnotification("did not join the memo ", $data['Notification']['reciever_id'], $data['Notification']['event_id'], 2, $data,true);
        }
        if ($ret = $this->Notification->delete($id)) {
            $res['flag'] = 'S';
        } else {
            $res['flag'] = 'F';
        }
        $res['msg'] = 'Notification removed';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

    public function api_acceptnotification($id = null) {
        $this->request->allowMethod('post', 'get');
        $this->Notification->id = $id;
        $data = $this->Notification->findById($id);
        if ($data['Notification']['type'] == '2') {
          $this->Contact->acceptContact($data);
            $response=$this->acceptRejectnotification("is now a contact", $data['Notification']['reciever_id'], 0, 3, $data);
            //echo " ";

        }
        if ($ret = $this->Notification->delete($id)) {
            $res['flag'] = 'S';
        } else {
            $res['flag'] = 'F';
        }
        $res['msg'] = 'Notification accepted';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

    public function api_acceptpendingcontact($id = null) {
        $this->request->allowMethod('post', 'get');
        $this->Notification->id = $id;
        $data = $this->Notification->findById($id);
        $query = "select * from contacts where (contact_id=" . $data['Notification']['sender_id'] . " AND user_id=" . $data['Notification']['reciever_id'] . ") OR (contact_id=" . $data['Notification']['reciever_id'] . " AND user_id=" . $data['Notification']['sender_id'] . ")";
        $rows = $this->Contact->query($query);
        if (empty($rows)) {
            $this->Contact->newContact($data);
        }
        if ($ret = $this->Notification->delete($id)) {
            $res['flag'] = 'S';
        } else {
            $res['flag'] = 'F';
        }
        $res['msg'] = 'Notification accepted';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

    public function api_accepteventnotification($id = null) {
        $this->request->allowMethod('post', 'get');
        $this->Notification->id = $id;
        $data = $this->Notification->findById($id);
        if ($data['Notification']['type'] == '1' || $data['Notification']['type'] == '4') {
            $this->acceptRejectnotification("joined the event ", $data['Notification']['reciever_id'], $data['Notification']['event_id'], 1, $data);
            $this->Event->acceptEvent($data);
        }
        if ($data['Notification']['type'] != '4') {
            if ($ret = $this->Notification->delete($id)) {
                $res['flag'] = 'S';
            } else {
                $res['flag'] = 'F';
            }
        } else {
            $res['flag'] = 'S';
        }

        $res['msg'] = 'Notification accepted';
        $res['document'] = $data;
        $this->setSerialize($res);
    }

    public function api_acceptmemonotification($id = null) {
        $this->request->allowMethod('post', 'get');
        $this->Notification->id = $id;
        $data = $this->Notification->findById($id);
        if ($data['Notification']['type'] == '3' || $data['Notification']['type'] == '5') {
            $this->acceptRejectnotification("joined the memo ", $data['Notification']['reciever_id'], $data['Notification']['event_id'], 2, $data);
            $this->Memo->acceptMemo($data);
        }
        //die;
        /* if ($ret = $this->Notification->delete()) {
          $res['flag'] = 'S';
          } else {
          $res['flag'] = 'F';
          } */
        if ($data['Notification']['type'] != '5') {
            if ($ret = $this->Notification->delete($id)) {
                $res['flag'] = 'S';
            } else {
                $res['flag'] = 'F';
            }
        } else {
            $res['flag'] = 'S';
        }
        $res['msg'] = 'Notification accepted';
        $res['document'] = $data;
        $this->setSerialize($res);
    }

    public function api_addeventinvitation() {
        $this->request->allowMethod('post', 'get');
        $notificationdata = array(
            'reciever_id' => $this->request->data('contact_id'),
            'title' => 'Event request',
            'sender_id' => $this->request->data('user_id'),
            'type' => '1',
            'ontime' => time(),
            'event_id' => $this->request->data('event_id'),
        );
        $ret = $this->Notification->save($notificationdata);
        $id = $this->Notification->getInsertId();
        $token = $this->Token->find('all', array('conditions' => array('user_id' => $this->request->data('contact_id'))));
        $event = $this->Event->findById($this->request->data('event_id'));
        $user = $this->User->findById($this->request->data('user_id'));
        foreach ($token as $val) {
            $notifyarray = array(
                'id' => $id,
                'device_type' => $val['Token']['type'],
                'message' => $user['User']['displayname'] . ' have send you request for ' . $event['Event']['title'],
                'deviceToken' => $val['Token']['token'],
            );
            $this->PushNotification->sendPush($notifyarray);
        }

        $res['flag'] = 'S';
        $res['msg'] = 'Contact saved';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

    public function api_addpending() {
        $this->request->allowMethod('post', 'get');
        $notificationdata = array(
            'reciever_id' => $this->request->data('contact_id'),
            'title' => 'Event request',
            'sender_id' => $this->request->data('user_id'),
            'type' => '4',
            'ontime' => time(),
            'event_id' => $this->request->data('event_id'),
        );
        $ret = $this->Notification->save($notificationdata);
        $id = $this->Notification->getInsertId();
        $token = $this->Token->find('all', array('conditions' => array('user_id' => $this->request->data('contact_id'))));
        $event = $this->Event->findById($this->request->data('event_id'));
        $user = $this->User->findById($this->request->data('user_id'));
        $reciever = $this->User->findById($this->request->data('contact_id'));
        foreach ($token as $val) {
            $notifyarray = array(
                'id' => $id,
                'device_type' => $val['Token']['type'],
                'message' => $user['User']['displayname'] . ' have send you request for ' . $event['Event']['title'],
                'deviceToken' => $val['Token']['token'],
            );
            $this->PushNotification->sendPush($notifyarray);
        }

        $res['flag'] = 'S';
        $res['msg'] = 'Contact saved';
        $res['document'] = $reciever;
        $this->setSerialize($res);
    }

    public function api_addpendingmemo() {
        $this->request->allowMethod('post', 'get');
        $notificationdata = array(
            'reciever_id' => $this->request->data('contact_id'),
            'title' => 'Memo request',
            'sender_id' => $this->request->data('user_id'),
            'type' => '5',
            'ontime' => time(),
            'event_id' => $this->request->data('memo_id'),
        );
        $ret = $this->Notification->save($notificationdata);
        $id = $this->Notification->getInsertId();
        $token = $this->Token->find('all', array('conditions' => array('user_id' => $this->request->data('contact_id'))));
        $event = $this->Memo->findById($this->request->data('memo_id'));
        $user = $this->User->findById($this->request->data('user_id'));
        $reciever = $this->User->findById($this->request->data('contact_id'));
        foreach ($token as $val) {
            $notifyarray = array(
                'id' => $id,
                'device_type' => $val['Token']['type'],
                'message' => $user['User']['displayname'] . ' have send you request for ' . $event['Memo']['title'],
                'deviceToken' => $val['Token']['token'],
            );
            $this->PushNotification->sendPush($notifyarray);
        }

        $res['flag'] = 'S';
        $res['msg'] = 'Contact saved';
        $res['document'] = $reciever;
        $this->setSerialize($res);
    }

    public function api_pendinglist() {
        $this->request->allowMethod('post', 'get');
        $data = $this->request->data;
        if ($data['type'] == 4) {
            $query = "select distinct users.id as userid,users.displayname,users.profileImg,users.username,users.email from notifications join users on notifications.reciever_id=users.id where type='" . $data['type'] . "' and sender_id='" . $data['user_id'] . "' and event_id='" . $data['event_id'] . "'";
        }
        if ($data['type'] == 5) {
            $query = "select distinct users.id as userid,users.displayname,users.profileImg,users.username,users.email from notifications join users on notifications.reciever_id=users.id where type='" . $data['type'] . "' and sender_id='" . $data['user_id'] . "' and event_id='" . $data['event_id'] . "'";
        }
        if ($data['type'] == 3) {
            $query = "select distinct users.id as userid,users.displayname,users.profileImg,users.username,users.email from notifications join users on notifications.reciever_id=users.id where type='" . 3 . "' and sender_id='" . $data['user_id'] . "' and event_id='" . $data['event_id'] . "'";
        }
        //echo $query;die;
        $ret = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'pending list';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

    public function api_addmemoinvitation() {
        $this->request->allowMethod('post', 'get');
        $notificationdata = array(
            'reciever_id' => $this->request->data('contact_id'),
            'title' => 'Memo request',
            'sender_id' => $this->request->data('user_id'),
            'type' => '3',
            'ontime' => time(),
            'event_id' => $this->request->data('memo_id'),
        );
        $ret = $this->Notification->save($notificationdata);
        $id = $this->Notification->getInsertId();
        $token = $this->Token->find('all', array('conditions' => array('user_id' => $this->request->data('contact_id'))));
        $event = $this->Memo->findById($this->request->data('memo_id'));
        $user = $this->User->findById($this->request->data('user_id'));
        foreach ($token as $val) {
            $notifyarray = array(
                'id' => $id,
                'device_type' => $val['Token']['type'],
                'message' => $user['User']['displayname'] . ' have send you request for ' . $event['Memo']['title'],
                'deviceToken' => $val['Token']['token'],
            );
            $this->PushNotification->sendPush($notifyarray);
        }

        $res['flag'] = 'S';
        $res['msg'] = 'Memo notification';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_clearall(){
      $user= $this->request->data('user');
      $query = "delete from notifications where reciever_id ='" . $user . "'";
      $ret = $this->Notification->query($query);
      $res['flag'] = 'S';
      $res['msg'] = 'All notifications removed.';
      $res['document'] = $ret;
      $this->setSerialize($res);
    }
    public function api_eventnotifications($sender, $event) {
        $query = "select reciever_id from notifications where type='1' and sender_id='" . $sender . "' and event_id='" . $event . "'";
        $ret = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Contact saved';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

    public function api_memonotifications($sender, $memo) {
        $query = "select reciever_id from notifications where type='3' and sender_id='" . $sender . "' and event_id='" . $memo . "'";
        $ret = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Contact saved';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

    public function api_unseen() {
        $user = $this->request->data('user');
        $query = "select COUNT(*) as total from notifications where reciever_id='$user' AND isview='0'";
        $ret = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Notification Count';
        $res['document'] = $ret[0][0];
        $this->setSerialize($res);
    }

    public function api_updateunseen() {
        $user = $this->request->data('user');
        $query = "Update notifications set isview='1' where reciever_id='$user' AND isview='0'";
        $ret = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Notification Count';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }

    public function api_sendEventChatnotification() {
        $this->autoRender = false;
        $details = $this->request->data('message');
        $user = $this->request->data('user');
        $event = $this->request->data('event');
        $type = $this->request->data('type');
        $evdetails = $this->Event->findById($event);
        $userdetails = $this->User->findById($user);
        $eveusersquery = "select * from events_users where event_id='" . $event . "' and user_id !='" . $user . "'";
        $eveusers = $this->Event->query($eveusersquery);
        foreach ($eveusers as $usr) {
            $insarray = array(
                'type' => 10,
                'reciever_id' => $usr['events_users']['user_id'],
                'title' => $userdetails['User']['displayname'] . " " . $details . " " . $evdetails['Event']['title'],
                'isview' => 0,
                'event_id' => $event, 'sender_id' => $user, 'ontime' => time());
            $this->Notification->save($insarray);
            $token = $this->Token->find('all', array('conditions' => array('user_id' => $usr['events_users']['user_id'])));
            foreach ($token as $val) {
                $notifyarray = array(
                    'id' => $event,
                    'device_type' => $val['Token']['type'],
                    'message' => $userdetails['User']['displayname'] . " " . $details . " " . $evdetails['Event']['title'],
                    'deviceToken' => $val['Token']['token'],
                );
                $this->PushNotification->sendPush($notifyarray);
            }
        }
    }

    public function api_sendMemoChatnotification() {
        $details = $this->request->data('message');
        $user = $this->request->data('user');
        $event = $this->request->data('event');
        $type = $this->request->data('type');
        $this->autoRender = false;
        $evdetails = $this->Memo->findById($event);
        $userdetails = $this->User->findById($user);
        $eveusersquery = "select * from memo_users where memo_id='" . $event . "' and user_id !='" . $user . "'";
        $eveusers = $this->Event->query($eveusersquery);
        foreach ($eveusers as $usr) {
            $insarray = array(
                'type' => 10,
                'reciever_id' => $usr['memo_users']['user_id'],
                'title' => $userdetails['User']['displayname'] . " " . $details . " " . $evdetails['Memo']['title'],
                'isview' => 0,
                'event_id' => $event, 'sender_id' => $user, 'ontime' => time());

            $this->Notification->save($insarray);
            $token = $this->Token->find('all', array('conditions' => array('user_id' => $usr['memo_users']['user_id'])));
            foreach ($token as $val) {
                $notifyarray = array(
                    'id' => $event,
                    'device_type' => $val['Token']['type'],
                    'message' => $userdetails['User']['displayname'] . " " . $details . " " . $evdetails['Memo']['title'],
                    'deviceToken' => $val['Token']['token'],
                );
                $this->PushNotification->sendPush($notifyarray);
            }
        }
    }

    public function acceptRejectnotification($details = "", $user = '', $event = "", $type = "", $notification = array(),$reject=false) {
        $userdetails = $this->User->findById($user);
        $evdetails = array();
        if ($type == 1) {
            $evdetails = $this->Event->findById($event);
            $eveusersquery = "select * from events_users where event_id='" . $event . "' and user_id !='" . $user . "'";
            $eveusers = $this->Event->query($eveusersquery);
        } elseif ($type == 2) {
            $evdetails = $this->Memo->findById($event);
            $eveusersquery = "select * from memo_users where memo_id='" . $event . "' and user_id !='" . $user . "'";
            $eveusers = $this->Memo->query($eveusersquery);
        }
        if ($type == 3) {
            $insarray = array(
                'type' => 7,
                'reciever_id' => $notification['Notification']['sender_id'],
                'title' => $userdetails['User']['displayname'] . " " . $details,
                'isview' => 0,
                'event_id' => 0, 'sender_id' => $user, 'ontime' => time());
            $this->Notification->save($insarray);
            $token = $this->Token->find('all', array('conditions' => array('user_id' => $notification['Notification']['sender_id'])));
            foreach ($token as $val) {
                $notifyarray = array(
                    'id' => 0,
                    'device_type' => $val['Token']['type'],
                    'message' => $userdetails['User']['displayname'] . " " . $details,
                    'deviceToken' => $val['Token']['token'],
                );
                $this->PushNotification->sendPush($notifyarray);
            }
        } elseif ($type == 2) {
            foreach ($eveusers as $usr) {

                if($reject){
                    if($usr['memo_users']['user_id']!=$notification['Notification']['sender_id']){
                        continue;
                    }
                }
                $this->Notification->create();
                $insarray = array(
                    'type' => 7,
                    'reciever_id' => $usr['memo_users']['user_id'],
                    'title' => $userdetails['User']['displayname'] . " " . $details . " " . $evdetails['Memo']['title'],
                    'isview' => 0,
                    'event_id' => $event, 'sender_id' => $user, 'ontime' => time());

                $this->Notification->save($insarray);
                $token = $this->Token->find('all', array('conditions' => array('user_id' => $usr['memo_users']['user_id'])));
                foreach ($token as $val) {
                    $notifyarray = array(
                        'id' => $event,
                        'device_type' => $val['Token']['type'],
                        'message' => $userdetails['User']['displayname'] . " " . $details . " " . $evdetails['Memo']['title'],
                        'deviceToken' => $val['Token']['token'],
                    );
                    if($val['Token']['token']!=''){
                        $this->PushNotification->sendPush($notifyarray);
                    }

                }
            }
        } elseif ($type == 1) {
            foreach ($eveusers as $usr) {

                if($reject){
                    if($usr['events_users']['user_id']!=$notification['Notification']['sender_id']){
                        continue;
                    }
                }
                $this->Notification->create();
                $insarray = array(
                    'type' => 7,
                    'reciever_id' => $usr['events_users']['user_id'],
                    'title' => $userdetails['User']['displayname'] . " " . $details . " " . $evdetails['Event']['title'],
                    'isview' => 0,
                    'event_id' => $event, 'sender_id' => $user, 'ontime' => time());

                $this->Notification->save($insarray);
                $token = $this->Token->find('all', array('conditions' => array('user_id' => $usr['events_users']['user_id'])));
                foreach ($token as $val) {
                    $notifyarray = array(
                        'id' => $event,
                        'device_type' => $val['Token']['type'],
                        'message' => $userdetails['User']['displayname'] . " " . $details . " " . $evdetails['Event']['title'],
                        'deviceToken' => $val['Token']['token'],
                    );
                    if($val['Token']['token']!=''){
                        $this->PushNotification->sendPush($notifyarray);
                    }
                }
            }
        }
        return true;
    }

}
