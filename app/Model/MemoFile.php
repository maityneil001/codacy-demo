<?php

App::uses('AppModel', 'Model');

/**
 * File Model
 *
 * @property Memo $Memo
 */
class MemoFile extends AppModel {

    public $actAs = array('Containable');
    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Memo' => array(
            'className' => 'Memo',
            'foreignKey' => 'memo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public function insertFiledata($link,$data) {
        $dataf=array(
            'memo_id'=>$data['memo'],
            'user_id'=>$data['user'],
            'link'=>$link,
            'created'=>  time(),
            'modified'=>  time(),
        );
        $this->create();
        return $this->save($dataf);
    }

}
