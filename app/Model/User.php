<?php

App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');

/**
 * User Model
 *
 * @property Country $Country
 * @property Contact $Contact
 * @property Event $Event
 * @property Note $Note
 * @property Event $Event
 */
class User extends AppModel {

    // The Associations below have been created with all possible keys, those that are not needed can be removed

    public $actsAs = array('Containable');
    //public $actsAs = array('Containable');

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Country' => array(
            'className' => 'Country',
            'foreignKey' => 'country',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Contact' => array(
            'className' => 'Contact',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Event' => array(
            'className' => 'Event',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Note' => array(
            'className' => 'Note',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'Event' => array(
            'className' => 'Event',
            'joinTable' => 'events_users',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'event_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
        )
    );

    public function checkLogin($email, $password) {
        $passwordHash = $password;
        $this->recursive = -1;
        $user = $this->find('first', array('conditions' => array('User.username' => $email), 'contain' => false));
        $res = array();

        if (!empty($user)) {
            $this->updateAll(array('need_sync'=>1,'event'=>1,'memo'=>1,'contact'=>1),array('User.id'=>$user['User']['id']));
            if ($user['User']['password'] == $passwordHash) {
                $res['flag'] = 'S';
                $res['msg'] = 'Authenticated!!';
                $res['document'] = $user;
            } else {
                $res['flag'] = 'E';
                $res['msg'] = 'Invalid username or password';
                $res['document'] = array();
            }
        } else {
            $res['flag'] = 'E';
            $res['msg'] = 'User do not exists!!';
            $res['document'] = array();
        }

        return $res;
    }

    public function register($data) {

        $dataU['User'] = $data;
        $res = array();
        if ($this->save($dataU)) {

            $data['id'] = $this->getLastInsertID();
            $res['flag'] = 'S';
            $res['msg'] = 'User data saved successfully!!';
            $res['document'] = $data;
        } else {

            $res['flag'] = 'E';
            $res['msg'] = 'User data could not be saved';
            $res['document'] = $data;
        }

        return $res;
    }
    public function update($data,$id) {
        $this->id=$id;
        $dataU['User'] = $data;
        $res = array();
        if ($this->save($dataU)) {
            $res['flag'] = 'S';
            $res['msg'] = 'User data saved successfully!!';
            $res['document'] = $data;
        } else {

            $res['flag'] = 'E';
            $res['msg'] = 'User data could not be saved';
            $res['document'] = $data;
        }

        return $res;
    }
    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        /*if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        } else if (isset($this->data[$this->alias]['update_password']) && !empty($this->data[$this->alias]['update_password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['update_password']);
        }*/

        // save our HABTM relationships
        foreach (array_keys($this->hasAndBelongsToMany) as $model) {
            if (isset($this->data[$this->name][$model])) {
                $this->data[$model][$model] = $this->data[$this->name][$model];
                unset($this->data[$this->name][$model]);
            }
        }

        return true;
    }

}
