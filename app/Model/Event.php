<?php

App::uses('AppModel', 'Model');

/**
 * Event Model
 *
 * @property User $User
 * @property Type $Type
 * @property File $File
 * @property Note $Note
 * @property User $User
 */
class Event extends AppModel {

    public $actAs = array('Containable');

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';


    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Type' => array(
            'className' => 'Type',
            'foreignKey' => 'type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'File' => array(
            'className' => 'File',
            'foreignKey' => 'event_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Note' => array(
            'className' => 'Note',
            'foreignKey' => 'event_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'User' => array(
            'className' => 'User',
            'joinTable' => 'events_users',
            'foreignKey' => 'event_id',
            'associationForeignKey' => 'user_id',
            'unique' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
        )
    );

    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        // save our HABTM relationships
        foreach (array_keys($this->hasAndBelongsToMany) as $model) {
            if (isset($this->data[$this->name][$model])) {
                $this->data[$model][$model] = $this->data[$this->name][$model];
                unset($this->data[$this->name][$model]);
            }
        }
    }

    public function createAndUpdateEvent($data, $id = null) {

        $dataE['Event'] = $data;
        if(isset($data['user_id'])){
           $dataE['Event']['User'] = $data['user_id']; 
        }
        $res = array();
        if ($id != null) {
            $this->id = $id;
        }
        if ($this->save($dataE)) {
            if ($id == null) {
                $data['id'] = $this->getLastInsertID();
            } else {
                $data['id'] = $id;
            }

            $res['flag'] = 'S';
            $res['msg'] = 'Event saved successfully!!';
            $res['document'] = $data;
        } else {
            $res['flag'] = 'E';
            $res['msg'] = 'Error saving event!!';
            $res['document'] = array();
        }

        return $res;
    }

    public function getTodayEvents() {

        $hour = 12;
        $today = strtotime($hour . ':00:00');
        $tomorrow = strtotime('+1 day', $today);

        $cond = array('Event.fromdate BETWEEN ? AND ?' => array($today, $tomorrow));
        $events = $this->find('all', array('conditions' => $cond));
        $res = array();
        $res['flag'] = 'S';
        $res['msg'] = 'Today Lists';
        $res['document'] = $events;
        return $res;
    }
    public function getAllEventsDatewise($fromdate,$todate) {
        $cond=array();
        if($fromdate!='' && $todate!=''){
            $cond = array('Event.fromdate BETWEEN ? AND ?' => array($fromdate, $todate));
        }elseif($fromdate!='' && $todate==''){
            
        }
        $events = $this->find('all', array('conditions' => $cond));
        $res = array();
        $res['flag'] = 'S';
        $res['msg'] = 'Datewise Lists';
        $res['document'] = $events;
        return $res;
    }
    public function acceptEvent($data) {
        //pr($data);
        $query="select * from events_users where event_id='".$data['Notification']['event_id']."' and user_id='".$data['Notification']['reciever_id']."'";
        $res=$this->query($query);
        //pr($res);die;
        if(empty($res)){
            $this->id=$data['Notification']['event_id'];
            $dataE['Event']['User'] = $data['Notification']['reciever_id'];
            return $this->save($dataE);
        }else{
           return true; 
        }
    }
    public function addeventuser($event,$user){
        $query="select * from events_users where event_id='".$event."' and user_id='".$user."'";
        $res=$this->query($query);
        if(empty($res)){
            $this->id=$event;
            $dataE['Event']['User'] =$user;
            return $this->save($dataE);
        }else{
            return true; 
        }
    }
    public function getAllEventsUserwise($fromdate,$todate,$user) {
        $cond=array();       
        $events = $this->find('all', array('conditions' => $cond));
        $res = array();
        $res['flag'] = 'S';
        $res['msg'] = 'Userevents Lists';
        $res['document'] = $events;
        return $res;
    }
    

}
