<?php 
	
	/**
	* 
	*/
	class ApiController extends AppController
	{
		
		public $uses = array('User');
		public $components = array('RequestHandler');

		public function beforeFilter(){
			parent::beforeFilter();	
			$this->autoRender=false;
			$this->layout = 'ajax';

		}

		public function activate(){
			
			$this->autoRender = false;
			$data['username'] = $this->request->query['username'];
			$email = $this->request->query['email'];
			if($this->_sendMail($email,'You are ready to go!!','account_activation',$data)){
				$response['success'] = 1;	
				$response['msg'] = 'Ivitation sent!!';
			}else{
				$response['success'] = 0;	
				$response['msg'] = 'Error sending the invitation';
			}
			
			$this->set(compact('response'));
			$this->set('_serialize', array('response'));
			$this->render('/JSON/response');
			return;
			
		}


		public function searchUser(){

			$this->autoRender=false;

			$keyword = strtolower($this->request->query['keyword']);
			$fb_id = $this->request->query['fb_id'];
			$contacts = json_decode($this->request->query['contacts'],true);

			$ids = array();


			if(!empty($contacts)){
				foreach($contacts as $k=>$val){
					$ids[] = $k;
				}
			}
			

			$ids[] = $fb_id;

			$ids = implode("','", $ids);	
			$ids = "'".$ids."'";

			// pr($keyword);
			// pr($fb_id);
			// pr($ids);
			// die;

			$query = "SELECT * FROM users WHERE (displayname LIKE '%".$keyword."%' OR username LIKE '%".$keyword."%' OR  email LIKE '%".$keyword."%') AND fb_id NOT IN(".$ids.")";

			$users = $this->User->query($query);
			$response['success'] = 1;
			$response['document'] = $users;
			// $this->set(compact('response'));
			// $this->set('_serialize', array('response'));
			// $this->render = '/JSON/response';
			echo json_encode($response);
			return;


		}


		public function reset(){
			$this->autoRender = false;
			$data['displayname'] = $this->request->query['displayname'];
			$data['username'] = $this->request->query['username'];
			$data['password'] = $this->request->query['password'];
			// $data['link'] = $this->request->query['link'];
			$email = $this->request->query['email'];
			if($this->_sendMail($email,'Almost there, and you are ready to go again!!','reset_password',$data)){
				$response['success'] = 1;	
				$response['msg'] = 'Ivitation sent!!';
			}else{
				$response['success'] = 0;	
				$response['msg'] = 'Error sending the invitation';
			}
			
			$this->set(compact('response'));
			$this->set('_serialize', array('response'));
			$this->render('/JSON/response');
			return;	
		}

		public function invite(){
			$this->autoRender = false;
			$data['username'] = $this->request->query['username'];
			$data['link'] = $this->request->query['link'];
			$email = $this->request->query['email'];
			if($this->_sendMail($email,'Invitation from TimeClip!!','invite_user',$data)){
				$response['success'] = 1;	
				$response['msg'] = 'Ivitation sent!!';
			}else{
				$response['success'] = 0;	
				$response['msg'] = 'Error sending the invitation';
			}
			
			$this->set(compact('response'));
			$this->set('_serialize', array('response'));
			$this->render('/JSON/response');
			return;
		}	

	}

 ?>