<?php

App::uses('AppModel', 'Model');

/**
 * Event Model
 *
 * @property User $User
 * @property Type $Type
 * @property File $File
 * @property Note $Note
 * @property User $User
 */
class Country extends AppModel {

    public $actAs = array('Containable');

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';
    public function countryList() {
        $cond = array();
        $countries = $this->find('all', array('conditions' => $cond));
        $res = array();
        $res['flag'] = 'S';
        $res['msg'] = 'Country Lists';
        $res['document'] = $countries;
        return $res;
    }

}
