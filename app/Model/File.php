<?php

App::uses('AppModel', 'Model');

/**
 * File Model
 *
 * @property Event $Event
 */
class File extends AppModel {

    public $actAs = array('Containable');
    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Event' => array(
            'className' => 'Event',
            'foreignKey' => 'event_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public function insertFiledata($link,$data) {
        $dataf=array(
            'event_id'=>$data['event'],
            'user_id'=>$data['user'],
            'link'=>$link,
            'created'=>  time(),
            'modified'=>  time(),
        );
        $this->create();
        return $this->save($dataf);
    }

}
