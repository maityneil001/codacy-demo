<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $countries = $this->User->Country->find('list');
        $events = $this->User->Event->find('list');
        $this->set(compact('countries', 'events'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $countries = $this->User->Country->find('list');
        $events = $this->User->Event->find('list');
        $this->set(compact('countries', 'events'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * api_index method
     *
     * @return void
     */
    public function api_index() {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

    /**
     * api_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function api_view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * api_add method
     *
     * @return void
     */
    public function api_add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $countries = $this->User->Country->find('list');
        $events = $this->User->Event->find('list');
        $this->set(compact('countries', 'events'));
    }

    /**
     * api_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function api_edit($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $countries = $this->User->Country->find('list');
        $events = $this->User->Event->find('list');
        $this->set(compact('countries', 'events'));
    }

    /**
     * api_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function api_delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function api_login() {
        if ($this->request->is('post')) {
            $email = $this->request->data('username');
            $password = $this->request->data('password');
            $res = $this->User->checkLogin($email, $password);
            $this->setSerialize($res);
        }
    }

    public function api_register() {
        if ($this->request->is('post')) {
            $res = $this->User->register($this->request->data);
            $this->setSerialize($res);
        }
    }
    public function uploadProfileImg() {
        $data=$this->request->data('data');
        $ret=$this->dataToImg('profile', $data);
        $res['flag'] = 'S';
        $res['msg'] = 'User details';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_checkunique() {
        if ($this->request->is('post')) {
            if($this->request->data('type')=='username'){
                $data=$this->User->findByUsername($this->request->data('value'));
            }
            if($this->request->data('type')=='email'){
                $data=$this->User->findByEmail($this->request->data('value'));
            }
            if($this->request->data('type')=='id'){
                $data=$this->User->findById($this->request->data('value'));
            }
            $res['flag'] = 'S';
            $res['msg'] = 'User details';
            $res['document'] = $data;
            $this->setSerialize($res);
        }
    }

}
