<?php

App::uses('Component', 'Controller');

class PushNotificationComponent extends Component {

    //public $UserNotification;


    public function __construct() {
        //$this->UserNotification = ClassRegistry::init('UserNotification');
    }

    public function sendPush($params = array()) {



        //print_r($params);

        extract($params);

        //echo WWW_ROOT ;die;
        // Put your device token here (without spaces):
        //$deviceToken = '5e8a9394c9da75fc3f8ed9e265a43e0d2c82a15d80be2e9d0b798b65df2f1dd2';
        // // Put your alert message here:
        //$message = 'My first push notification!';


        if ($device_type == 1) {

            // // Put your private key's passphrase here:
            $passphrase = '123456';
            //$passphrase = '1234';
            ////////////////////////////////////////////////////////////////////////////////

            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', WWW_ROOT . 'cert/pushcert.pem');
            //stream_context_set_option($ctx, 'ssl', 'local_cert', WWW_ROOT.'cert/Pushkack.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

            // Open a connection to the APNS server
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

            if (!$fp)
                exit("Failed to connect: $err $errstr" . PHP_EOL);

            //echo 'Connected to APNS' . PHP_EOL;
            // Create the payload body
            $body['aps'] = array(
                'alert' => $message,
                'sound' => 'default'
            );

            $body['params'] = array(
                //'type' => $type,
                'id' => $id
            );
            // Encode the payload as JSON
            $payload = json_encode($body);

            // Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

            // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));

            if ($result) {

                $response['flag'] = 's';
                $response['message'] = 'Notification Sent';
                //$response['type'] = $type;
                $response['id'] = $id;
            } else {
                $response['flag'] = 'e';
                $response['message'] = 'Notification Not Sent';
            }

            // Close the connection to the server
            fclose($fp);

            //echo json_encode($response);
            return $response;
        } else if ($device_type == 0) {
            //die('in android');
            // Set POST variables
            $url = 'https://android.googleapis.com/gcm/send';


            $fields = array(
                'registration_ids' => array($deviceToken),
                'data' => array('message' => $message, 'id' => $id)
            );
            $headers = array(
                'Authorization: key=AIzaSyDQmmB57hOwjyWQO2RDlsM7hoMO81kwzMM',
                'Content-Type: application/json'
            );
            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Execute post
            $result = curl_exec($ch);
            //pr($deviceToken);die;
            if ($result === FALSE) {
                
            } else {
                
            }

            // Close connection
            curl_close($ch);
        }
    }

}
