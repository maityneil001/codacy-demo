<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $countries = $this->User->Country->find('list');
        $events = $this->User->Event->find('list');
        $this->set(compact('countries', 'events'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $countries = $this->User->Country->find('list');
        $events = $this->User->Event->find('list');
        $this->set(compact('countries', 'events'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * api_index method
     *
     * @return void
     */
    public function api_index() {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

    /**
     * api_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function api_view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * api_add method
     *
     * @return void
     */
    public function api_add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $countries = $this->User->Country->find('list');
        $events = $this->User->Event->find('list');
        $this->set(compact('countries', 'events'));
    }

    /**
     * api_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function api_edit($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $countries = $this->User->Country->find('list');
        $events = $this->User->Event->find('list');
        $this->set(compact('countries', 'events'));
    }

    /**
     * api_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function api_delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function api_login() {
        if ($this->request->is('post')) {
            $email = $this->request->data('username');
            $password = $this->request->data('password');
            $res = $this->User->checkLogin($email, $password);
            $this->setSerialize($res);
        }
    }

    public function api_register() {
        if ($this->request->is('post')) {
            $res = $this->User->register($this->request->data);
            $this->setSerialize($res);
        }
    }
    public function api_update($id) {
        if ($this->request->is('post')) {
            $res = $this->User->update($this->request->data,$id);
            $this->setSerialize($res);
        }
    }

    public function api_uploadProfileImg() {
        $data = $this->request->data('dataitem');
        $ret = $this->dataToImg('profile', $data);
        $res['flag'] = 'S';
        $res['msg'] = 'User details';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_updatesync(){
      $user=$this->request->data('user');
      $type=$this->request->data('type');
      $userdetails=$this->User->findById($user);
      if($type=='event'){
        if($userdetails['User']['memo']!=1 && $userdetails['User']['contact']!=1){
          $this->User->query("update users set event=0,need_sync=0 where id=".$user);
        }else{
          $this->User->query("update users set event=0 where id=".$user);
        }
        $ret='update event';
      }elseif ($type=='memo') {
        if($userdetails['User']['event']!=1 && $userdetails['User']['contact']!=1){
          $this->User->query("update users set memo=0,need_sync=0 where id=".$user);
        }else{
          $this->User->query("update users set memo=0 where id=".$user);
        }
        $ret='update memo';
      }elseif ($type=='contact') {
        if($userdetails['User']['memo']!=1 && $userdetails['User']['event']!=1){
          $this->User->query("update users set contact=0,need_sync=0 where id=".$user);
        }else{
          $this->User->query("update users set contact=0 where id=".$user);
        }
        $ret='update contact';
      }
      $res['flag'] = 'S';
      $res['msg'] = 'Update details';
      $res['document'] = $ret;
      $this->setSerialize($res);
    }
    public function api_checkunique() {
        if ($this->request->is('post')) {
            if ($this->request->data('type') == 'username') {
                $data = $this->User->findByUsername($this->request->data('value'));
            }
            if ($this->request->data('type') == 'email') {
                $data = $this->User->findByEmail($this->request->data('value'));
            }
            if ($this->request->data('type') == 'id') {
                $data = $this->User->findById($this->request->data('value'));
            }
            $res['flag'] = 'S';
            $res['msg'] = 'User details';
            $res['document'] = $data;
            $this->setSerialize($res);
        }
    }

    public function api_searchuser() {
        //$this->autoRender = false;
        $keyword = strtolower($this->request->query['keyword']);
        $fb_id = $this->request->query['fb_id'];
        $contacts = json_decode($this->request->query['contacts'], true);
        $type=$this->request->query['type'];
        $ids = array();
        if (!empty($contacts)) {
            foreach ($contacts as $k => $val) {
                $ids[] = $k;
            }
        }
        $ids[] = $fb_id;
        $ids = implode("','", $ids);
        $ids = "'" . $ids . "'";
        if($type == 1){
          $query = "SELECT * FROM users WHERE (displayname LIKE '%" . $keyword . "%' OR username LIKE '%" . $keyword . "%' OR  email LIKE '%" . $keyword . "%') AND id NOT IN(SELECT distinct(reciever_id) from notifications where sender_id='".$fb_id."'and type='2') AND id !='".$fb_id."' AND id NOT IN (SELECT contact_id from contacts where user_id='".$fb_id."') AND id NOT IN (SELECT user_id from contacts where contact_id='".$fb_id."')";
        }elseif($type == 2){
          $query = "SELECT * FROM users WHERE (displayname LIKE '%" . $keyword . "%' OR username LIKE '%" . $keyword . "%' OR  email LIKE '%" . $keyword . "%') AND id NOT IN(SELECT distinct(reciever_id) from notifications where sender_id='".$fb_id."' and type='4' and event_id='".$this->request->query['event_id']."') AND id !='".$fb_id."' AND id NOT IN (SELECT contact_id from contacts where user_id='".$fb_id."') AND id NOT IN (SELECT user_id from contacts where contact_id='".$fb_id."')";
        }elseif($type == 3){
          $query = "SELECT * FROM users WHERE (displayname LIKE '%" . $keyword . "%' OR username LIKE '%" . $keyword . "%' OR  email LIKE '%" . $keyword . "%') AND id NOT IN(SELECT distinct(reciever_id) from notifications where sender_id='".$fb_id."' and type='5' and event_id='".$this->request->query['memo_id']."') AND id !='".$fb_id."' AND id NOT IN (SELECT contact_id from contacts where user_id='".$fb_id."') AND id NOT IN (SELECT user_id from contacts where contact_id='".$fb_id."')";
        }

        //pr($query);
        $users = $this->User->query($query);
        //pr($users);
        $response['document']=$users;
        $response['success'] = 1;
        $this->setSerialize($response);
        //echo json_encode($response);
        //return;
    }
    public function api_usersbyid() {
        $ids=implode(",",$this->request->data('userids'));
        $query = "SELECT * FROM users WHERE id  IN(" . $ids . ")";
        $users = $this->User->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'User details';
        $res['document'] = $users;
        $this->setSerialize($res);

    }
    public function api_registerToken() {
        $data=$this->request->data;
        $data['created']=time();
        $query = "SELECT * FROM tokens WHERE user_id='".$data['user_id']."' AND token='".$data['token']."' AND type='".$data['type']."' ";
        $token = $this->Token->query($query);
        if(count($token)==0){
           $ret=$this->Token->save($data);
        }else{
            $ret=FALSE;
        }
        $res['flag'] = 'S';
        $res['msg'] = 'Register token';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_notificationlist($id) {
        $data=$this->request->data;
        $data['created']=time();
        $query = "SELECT notifications.id,notifications.title,users.username,users.displayname,users.profileImg FROM notifications JOIN users on notifications.sender_id=users.id WHERE reciever_id='".$id."' AND type='".$data['type']."' order by notifications.id desc";
        $token = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Notification list';
        $res['document'] = $token;
        $this->setSerialize($res);
    }
    public function api_eventnotificationlist($id) {
        $data=$this->request->data;
        $data['created']=time();
        $query = "SELECT notifications.id,notifications.title,users.username,users.displayname,users.profileImg,events.title,events.fromdate,events.fromtime FROM notifications JOIN users on notifications.sender_id=users.id JOIN events on notifications.event_id=events.id WHERE notifications.reciever_id='".$id."' AND notifications.type='".$data['type']."' order by notifications.id desc";
        $token = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Event notification list';
        $res['document'] = $token;
        $this->setSerialize($res);
    }
    public function api_pendeventnotifications($id) {
        $data=$this->request->data;
        $data['created']=time();
        $query = "SELECT notifications.id,notifications.title,users.username,users.displayname,users.profileImg,events.title,events.fromdate,events.fromtime FROM notifications JOIN users on notifications.sender_id=users.id JOIN events on notifications.event_id=events.id WHERE notifications.reciever_id='".$id."' AND notifications.type='".$data['type']."' order by notifications.id desc";
        $token = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Pending notification list';
        $res['document'] = $token;
        $this->setSerialize($res);
    }
    public function api_memonotificationlist($id) {
        $data=$this->request->data;
        $data['created']=time();
        $query = "SELECT notifications.id,notifications.title,users.username,users.displayname,users.profileImg,memos.title,memos.category FROM notifications JOIN users on notifications.sender_id=users.id JOIN memos on notifications.event_id=memos.id WHERE notifications.reciever_id='".$id."' AND notifications.type='".$data['type']."' order by notifications.id desc";
        $token = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Memo notification list';
        $res['document'] = $token;
        $this->setSerialize($res);
    }
    public function api_pendmemonotifications($id) {
        $data=$this->request->data;
        $data['created']=time();
        $query = "SELECT notifications.id,notifications.title,users.username,users.displayname,users.profileImg,memos.title,memos.category FROM notifications JOIN users on notifications.sender_id=users.id JOIN memos on notifications.event_id=memos.id WHERE notifications.reciever_id='".$id."' AND notifications.type='".$data['type']."' order by notifications.id desc";
        $token = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Memo notification list';
        $res['document'] = $token;
        $this->setSerialize($res);
    }
    public function api_pendingcontacts() {
        $data=$this->request->data;
        $data['created']=time();
        $query = "SELECT reciever_id,notifications.id,notifications.title,users.username, users.id as userid,users.displayname,users.profileImg FROM notifications JOIN users on notifications.reciever_id=users.id  WHERE notifications.sender_id='".$data['user_id']."' AND (notifications.type='2' || notifications.type='4' || notifications.type='5') group by reciever_id order by notifications.id desc";
        $token = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Pendng Contacts';
        $res['document'] = $token;
        $this->setSerialize($res);
    }
    public function api_allnotification($id){
        $query = "SELECT notifications.id,notifications.title,notifications.event_id,notifications.type,users.username,users.displayname,users.profileImg,events.title,events.fromdate,events.fromtime,memos.title as memotitle,memos.category as memocat FROM notifications JOIN users on notifications.sender_id=users.id LEFT JOIN events on notifications.event_id=events.id LEFT JOIN memos on notifications.event_id=memos.id WHERE reciever_id='".$id."' order by notifications.id desc";
        $token = $this->Notification->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Notification list';
        $res['document'] = $token;
        $this->setSerialize($res);
    }


}
