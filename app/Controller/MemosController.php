<?php

App::uses('AppController', 'Controller');
App::uses('Emoji', 'Lib');

/**
 * Memos Controller
 *
 * @property Memo $Memo
 * @property PaginatorComponent $Paginator
 */
class MemosController extends AppController {

    public $uses=array('Event','MemoFile','MemoNote','Memo','Notification');


    public function beforeFilter() {
        parent::beforeFilter();
    }
    public $virtualFields=array(
        'createdby'=>'Memos.user_id'
    );
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator','PushNotification');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Memo->recursive = 0;
        $this->set('events', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Memo->exists($id)) {
            throw new NotFoundException(__('Invalid event'));
        }
        $options = array('conditions' => array('Memo.' . $this->Memo->primaryKey => $id));
        $this->set('event', $this->Memo->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Memo->create();
            if ($this->Memo->save($this->request->data)) {
                $this->Flash->success(__('The event has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The event could not be saved. Please, try again.'));
            }
        }
        $users = $this->Memo->User->find('list');
        $types = $this->Memo->Type->find('list');
        $users = $this->Memo->User->find('list');
        $this->set(compact('users', 'types', 'users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Memo->exists($id)) {
            throw new NotFoundException(__('Invalid event'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Memo->save($this->request->data)) {
                $this->Flash->success(__('The event has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The event could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Memo.' . $this->Memo->primaryKey => $id));
            $this->request->data = $this->Memo->find('first', $options);
        }
        $users = $this->Memo->User->find('list');
        $types = $this->Memo->Type->find('list');
        $users = $this->Memo->User->find('list');
        $this->set(compact('users', 'types', 'users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Memo->id = $id;
        if (!$this->Memo->exists()) {
            throw new NotFoundException(__('Invalid event'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Memo->delete()) {
            $this->Flash->success(__('The event has been deleted.'));
        } else {
            $this->Flash->error(__('The event could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function api_create() {
        if ($this->request->is('post')) {
            if($this->request->data('id')){
                $evdetails = $this->Memo->findById($this->request->data('id'));
                if($this->request->data('title')!=$evdetails['Memo']['title']){
                  $msg="renamed the memo ";
                  $msg1 = $evdetails['Memo']['title']." to ".$this->request->data('title');
                }
                $res = $this->Memo->createAndUpdateMemo($this->request->data,$this->request->data('id'));
                //$this->sendUpdatenotification("updated details of memo ","",$this->request->data('id'));
                $this->sendUpdatenotification($msg,"",$this->request->data('id'),$msg1);
            }else{
                $res = $this->Memo->createAndUpdateMemo($this->request->data);
            }
            $this->setSerialize($res);
        }
    }

    public function api_forToday() {
        $res = $this->Memo->getTodayMemos();
        $this->setSerialize($res);
    }
    public function api_allMemos() {
        $fromdate = $this->request->data('fromdate');
        $todate = $this->request->data('todate');
        $res = $this->Memo->getAllMemosDatewise($fromdate,$todate);
        $this->setSerialize($res);
    }
    public function api_memoDetails($id) {
        //$this->Memo->id=$id;
        $ret=$this->Memo->findById($id);
        $res['flag'] = 'S';
        $res['msg'] = 'Memo details';
        $res['document'] = $ret;
        $this->setSerialize($res);
    }
    public function api_uploadMemoImg() {
        $data=$this->request->data('dataitem');
        $ret=$this->dataToImg('events', $data['image']);
        $this->MemoFile->insertFiledata($ret,$data);
        $this->sendUpdatenotification("added new image to memo ",$data['user'],$data['memo']);
        $res['flag'] = 'S';
        $res['msg'] = 'Memo Image';
        $res['document'] = $ret;

        $this->setSerialize($res);
    }
    public function api_memousers($id) {
        $query = "SELECT * from memo_users where memo_id='".$id."' ";
        $token = $this->Memo->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Memo users list';
        $res['document'] = $token;
        $this->setSerialize($res);
    }
    public function api_memofiles($id) {
         $query = "SELECT * from memo_files where memo_id='".$id."' ";
        // echo $query;die;
        $files = $this->Memo->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Memo files list';
        $res['document'] = $files;
        $this->setSerialize($res);
    }
    public function api_delete($id = null) {
        $this->Memo->id = $id;
        $this->request->allowMethod('post', 'delete');
        $this->sendUpdatenotification("deleted the memo ","",$id);
        $query="delete from notifications where event_id='$id' and (type='3' or type='5')";
        $this->Notification->query($query);
        if ($this->Memo->delete()) {
            /*$files="Delete from memo_files where memo_id='".$id."'";
            $users="delete from memo_users where memo_id='".$id."'";
            $notes="delete from memo_notes where memo_id='".$id."'";
            $this->Memo->query($files);
            $this->Memo->query($users);
            $this->Memo->query($notes);*/
            $res['flag'] = 'S';
            $res['msg'] = 'Memo deleted';
            $res['document'] = $id;
        } else {
            $res['flag'] = 'E';
            $res['msg'] = 'Memo not deleted';
            $res['document'] = false;
        }
        $this->setSerialize($res);
    }
    public function api_deletefile() {
        $id=$this->request->data('id');
        $memo=$this->request->data('memo');
        $user=$this->request->data('user');
        $query="Delete from memo_files where id='".$id."'";
        //$this->request->allowMethod('post', 'delete');
        $this->MemoFile->id=$id;
        $details=" deleted a file from the memo ";
        if ($this->MemoFile->delete()) {
          $this->sendUpdatenotification($details,$user,$memo);
            $res['flag'] = 'S';
            $res['msg'] = 'File deleted';
            $res['document'] = $id;
        } else {
            $res['flag'] = 'E';
            $res['msg'] = 'File could not be deleted';
            $res['document'] = false;
        }
        $this->setSerialize($res);
    }
    public function api_deletememouser($userid = null,$memoid = null) {
        $query="delete from memo_users where user_id='".$userid."' and memo_id='".$memoid."'";
        $this->Memo->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'User deleted';
        $res['document'] = $userid;
        $this->setSerialize($res);
    }
    public function api_getallusermemo() {
        $data=$this->request->data;
        //$fromdate = $this->request->data('fromdate');
        //$todate = $this->request->data('todate');
        $user = $this->request->data('user');
        $query="SELECT memos.* FROM memos left join memo_users on memos.id=memo_users.memo_id where  memo_users.user_id='" . $user . "'";
        //echo $query;die();
        $memos = $this->Memo->query($query);
        $res['flag'] = 'S';
        $res['msg'] = 'Usermemos Lists';
        $res['document'] = $memos;
        $this->setSerialize($res);
    }
    public function api_addupdatenote() {
        $data=$this->request->data;
        if(isset($data['id']) && $data['id']!=''){
            $this->MemoNote->id=$data['id'];
        }
        $text=Emoji::Encode($data['note']);
        $notes=$this->MemoNote->save(array('memo_id'=>$data['memo_id'],'user_id'=>$data['user_id'],'note'=>$text));
        $this->sendUpdatenotification("updated notes for the memo",$data['user_id'],$data['memo_id']);
        $res['flag'] = 'S';
        $res['msg'] = 'Note saved';
        $res['document'] = $notes;
        $this->setSerialize($res);
    }
    public function api_getallnote($id=null) {
       $eventnote = "SELECT users.id,memo_notes.id as noteid,users.username,users.displayname,memo_notes.note FROM memo_notes JOIN users ON memo_notes.user_id=users.id where memo_notes.memo_id='" .$id. "'";
       $notes=$this->MemoNote->query($eventnote);
       $notes=$this->Note->query($eventnote);
       foreach ($notes as $key => $value) {
         //print_r($value);die;
         $notes[$key]['notes']['note']=Emoji::Decode($value['notes']['note']);
       }
       $res['flag'] = 'S';
       $res['msg'] = 'Note list';
       $res['document'] = $notes;
       $this->setSerialize($res);
    }
    public function api_convertextraparams() {
       $data=$this->request->data;
       /***notes****/
       $memonote = "SELECT * FROM notes  where event_id='" .$data['event']. "'";
       $notes=$this->Note->query($memonote);
       $this->Note->query("DELETE  FROM notes  where event_id='" .$data['event']. "'");
       if(!empty($notes)){
           foreach ($notes as $value) {
                $value['notes']['memo_id']=$data['memo'];
                $this->MemoNote->save($value['notes']);
            }
       }

       /*****images****/
       $memofiles ="SELECT * from files where event_id='".$data['event']."' ";
       $files=$this->File->query($memofiles);
       $this->File->query("DELETE  FROM files  where event_id='" .$data['event']. "'");
       if(!empty($files)){
        foreach ($files as $value) {
            $value['files']['memo_id']=$data['memo'];
            $this->MemoFile->save($value['files']);
        }
       }
       /*****users****/
       $memousers = "SELECT * FROM events_users  where event_id='" .$data['event']. "'";
       $users=$this->Event->query($memousers);
       foreach ($users as $value) {
           $this->User->query("UPDATE  `users` SET  `need_sync` =  '1',`memo` =  '1' WHERE  `users`.`id` =".$value['events_users']['user_id']);
           $this->Memo->addmemouser($data['memo'],$value['events_users']['user_id']);
       }
       $this->Event->query("DELETE  FROM events_users  where event_id='" .$data['event']. "'");
       //die;
       $this->Event->query("DELETE  FROM events  where id='" .$data['event']. "'");
       $nquery="update notifications set event_id='".$data['memo']."',type='3' where event_id='".$data['event']."' and type='1'";
       $this->Notification->query($nquery);
       $nquery1="update notifications set event_id='".$data['memo']."',type='5' where event_id='".$data['event']."' and type='4'";
       $this->Notification->query($nquery1);
       $res['flag'] = 'S';
       $res['msg'] = 'Updated';
       $res['document'] = true;
       $this->setSerialize($res);
    }

    public function api_leaveMemo(){
        $memo=$this->request->data('memo');
        $user=$this->request->data('user');
        $evdetails = $this->Memo->findById($memo);
        $userdetails = $this->User->findById($user);
        $details=" has left the memo ";
        $this->Memo->query("delete from memo_users where user_id=$user and memo_id=$memo");
        $this->sendUpdatenotification($details,$user,$memo);
        /*$insarray=array(
              'type'=>10,
              'reciever_id'=>$evdetails['Memo']['user_id'],
              'title'=>$userdetails['User']['displayname']." " . $details." " . $evdetails['Memo']['title'] ,
              'isview'=>0,
              'event_id'=>$memo,'sender_id'=>$user,'ontime'=>time());
          $this->Notification->save($insarray);
          $token = $this->Token->find('all', array('conditions' => array('user_id' => $evdetails['Memo']['user_id'])));
          foreach ($token as $val) {
              $notifyarray = array(
                  'id' => $memo,
                  'device_type' => $val['Token']['type'],
                  'message' => $userdetails['User']['displayname']." " . $details." " . $evdetails['Memo']['title'],
                  'deviceToken' => $val['Token']['token'],
              );
              $this->PushNotification->sendPush($notifyarray);
          }*/
          $res['flag'] = 'S';
          $res['msg'] = 'Left event';
          $res['document'] = true;
          $this->setSerialize($res);
    }
    public function sendUpdatenotification($details, $user, $memo , $msg="") {
        //$this->Event->id=$event;
        //$this->autoRender=false;
        $evdetails = $this->Memo->findById($memo);
        if($user==""){
          $user=$evdetails['Memo']['user_id'];
        }
        $userdetails = $this->User->findById($user);
        //pr($userdetails);die;
        $eveusersquery = "select * from memo_users where memo_id='" . $memo . "' and user_id !='" . $user . "'";
        //$eveusersquery = "select * from memo_users where memo_id='" . $memo."'";
        $eveusers = $this->Memo->query($eveusersquery);
        foreach ($eveusers as $usr) {
          if($details=="renamed the memo "){
            $notifyMsg=$userdetails['User']['displayname']." " . $details." " . " " .$msg;
          }else{
            $notifyMsg=$userdetails['User']['displayname']." " . $details." " . $evdetails['Memo']['title'] . " " .$msg;
          }
          $insarray=array(
                'type'=>8,
                'reciever_id'=>$usr['memo_users']['user_id'],
                'title'=>$notifyMsg,
                'isview'=>0,
                'event_id'=>$memo,'sender_id'=>$user,'ontime'=>time());
          if(strpos($details,'deleted')!== false){
            $insarray['type']=9;

          }
          $this->Notification->save($insarray);
            $token = $this->Token->find('all', array('conditions' => array('user_id' => $usr['memo_users']['user_id'])));
            foreach ($token as $val) {
                $notifyarray = array(
                    'id' => $memo,
                    'device_type' => $val['Token']['type'],
                    'message' => $notifyMsg,
                    'deviceToken' => $val['Token']['token'],
                );
                $this->PushNotification->sendPush($notifyarray);
            }
        }
        return true;
    }
    public function api_sendUpdatenotification($details, $user, $memo , $msg="",$event) {
        //$this->Event->id=$event;
        $this->autoRender=false;
        $evdetails = $this->Memo->findById($memo);
        if($user==""){
          $user=$evdetails['Memo']['user_id'];
        }
        $userdetails = $this->User->findById($user);
        //pr($userdetails);die;
        $eveusersquery = "select * from memo_users where memo_id='" . $memo . "' and user_id !='" . $user . "'";
        //$eveusersquery = "select * from memo_users where memo_id='" . $memo."'";
        $eveusers = $this->Memo->query($eveusersquery);
        foreach ($eveusers as $usr) {
          $this->User->query("update users set need_sync=1,memo=1 where id=".$usr['memo_users']['user_id']);
          if($details=="renamed the memo "){
            $notifyMsg=$userdetails['User']['displayname']." " . $details." " . " " .$msg;
          }else{
            $notifyMsg=$userdetails['User']['displayname']." " . $details." " . $evdetails['Memo']['title'] . " " .$msg;
          }
          $insarray=array(
                'type'=>11,
                'reciever_id'=>$usr['memo_users']['user_id'],
                'title'=>$notifyMsg ,
                'isview'=>0,
                'event_id'=>$event,'sender_id'=>$user,'ontime'=>time());
            $this->Notification->save($insarray);
            $token = $this->Token->find('all', array('conditions' => array('user_id' => $usr['memo_users']['user_id'])));
            foreach ($token as $val) {
                $notifyarray = array(
                    'id' => $event,
                    'device_type' => $val['Token']['type'],
                    'message' => $notifyMsg,
                    'deviceToken' => $val['Token']['token'],
                );
                $this->PushNotification->sendPush($notifyarray);
            }
        }
        return true;
    }

    public function api_sendConvertnotification() {
        //$this->Event->id=$event;
        $details=$this->request->data('details');
        $user=$this->request->data('user');
        $memo=$this->request->data('memo');
        $msg=$this->request->data('msg');
        $event = $this->request->data('event');
        $this->autoRender=false;
        $evdetails = $this->Memo->findById($memo);
        if($user==""){
          $user=$evdetails['Memo']['user_id'];
        }
        $userdetails = $this->User->findById($user);
        //pr($userdetails);die;
        $eveusersquery = "select * from memo_users where memo_id='" . $memo . "' and user_id !='" . $user . "'";
        //$eveusersquery = "select * from memo_users where memo_id='" . $memo."'";
        $eveusers = $this->Memo->query($eveusersquery);
        foreach ($eveusers as $usr) {
          $insarray=array(
                'type'=>11,
                'reciever_id'=>$usr['memo_users']['user_id'],
                'title'=>$userdetails['User']['displayname']." " . $details." " . $evdetails['Memo']['title'] . " " .$msg,
                'isview'=>0,
                'event_id'=>$event,'sender_id'=>$user,'ontime'=>time());
            $this->Notification->save($insarray);
            $token = $this->Token->find('all', array('conditions' => array('user_id' => $usr['memo_users']['user_id'])));
            foreach ($token as $val) {
                $notifyarray = array(
                    'id' => $event,
                    'device_type' => $val['Token']['type'],
                    'message' => $userdetails['User']['displayname']." " . $details." " . $evdetails['Memo']['title'] . " " .$msg,
                    'deviceToken' => $val['Token']['token'],
                );
                $this->PushNotification->sendPush($notifyarray);
            }
        }
        $res['flag'] = 'S';
        $res['msg'] = 'Updated';
        $res['document'] = true;
        $this->setSerialize($res);
    }

}
